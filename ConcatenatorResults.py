__author__ = 'kbessonov'

import sys,re, os, linecache
from optparse import OptionParser
from collections import Counter

def setupArguments():

    Parser = OptionParser()
    Parser.add_option('-d', action='store', dest='dir', help="Absolute path to directory")
    Parser.add_option('-o', action='store', dest='file_out', help="Output concatinated file name")


    return Parser.parse_args()[0]


if __name__ == "__main__":
    print "Welcome to header-based concatinator"
    args = setupArguments()
    dirfiles = [f for f in os.listdir(args.dir)]


    print str(len(dirfiles))+" files in input dir: "+str(", ".join(dirfiles)+"\n")
    #print args.file_out
    headers=[]
    for f_in in dirfiles:
        #print args.dir+f_in
        try:
            open(args.dir+f_in, "r")
        except IOError:
            sys.exit("ERROR: Could not open \""+str(args.dir+f_in)+"\" file. Check input path (parameter -d)")
        h=linecache.getline(args.dir+f_in, 1)
        headers.append(h)
    #print headers
    #print linecache.getline("./RESULTS/CCDBION219_NGS1_Diatoms_2016-04-05-CYA359F-781R-deg_IonXpress_004.fastq.fasta_BLAST.txt.out", 1)

    common_header = Counter([h for h in headers]).most_common(1)[0][0]
    if len(common_header) < 5:
        sys.exit("ERROR: Files do not contain a COMMON header on line 1! Or input directory does not end with \"/\" symbol")
    #print Counter([h for h in headers]).most_common(1)

    fp_out = open(args.file_out,"w"); fp_out.write(common_header); fp_out.close()
    fp_out = open(args.file_out, "a")
    for f_in in dirfiles:
        header = linecache.getline(args.dir+f_in, 1)
        if re.compile("OTU").match(header):
            n_lines = sum(1 for line in open(args.dir+f_in))+1
            print "Concatenating \""+f_in+ "\" file to \""+str(fp_out.name)+"\""
            [fp_out.write(linecache.getline(args.dir+f_in,ln)) for ln in range(2, n_lines)]
        else:
            continue
    print "\nConcatenated file found at \""+str(args.file_out)+"\""
    fp_out.close()
