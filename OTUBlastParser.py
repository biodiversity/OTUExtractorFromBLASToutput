__author__ = 'kbessonov'

import sys,time,re,csv, linecache
from optparse import OptionParser
from collections import Counter
from collections import OrderedDict
from decimal import Decimal

from itertools import islice


def setupArguments():

    Parser = OptionParser()
    Parser.add_option('-i', action='store', dest='blastfile', help="BLAST input file to parse")
    Parser.add_option('-o', action='store', dest='resultfile', help="Tab-delimited results file")
    Parser.add_option('-r', action='store', dest='metadatafile', help="Meta data file with taxonomy info per sample ID")

    return Parser.parse_args()[0]

def ParseTextInput(ln_start, ln_end, fp_pos_start, fp_pos_end):
    data_annotator_dict={}
    #data = [linecache.getline(options.blastfile,ln) for ln in range(ln_start,ln_end)]

    fp=open(options.blastfile, "rU"); fp.seek(fp_pos_start)
    data = fp.readlines(fp_pos_end-fp_pos_start)
    data = data[0:ln_end-ln_start]
    #print len(data), ln_end,  ln_start

    #print data
    #print fp_pos_end, fp_pos_start
    #sys.exit("End")
    #print fp.readlines(1)
    #fp.seek(fp_pos_end)
    #print fp.readlines(1)
    #sys.exit("End")
    #print ln_start,ln_end
    #data = list(islice(fp, (ln_end-ln_start)+10))
    #print ln_end-ln_start+100
    #linecache.clearcache()

    #print data
    #print "List len"+str(len(data))
    idx_OTU_name =  [i for i in range(0, len(data)) if re.compile("(.*)(OTU_\d+)(;.*)").match(data[i])];
    idx_OTU_name.append(len(data))
    if len(data) < 10:
        sys.exit("ERROR: Read too few input data lines")
    #print idx_OTU_name
    #sys.exit()
    #print len(idx_OTU_name)

    for i in range(0,len(idx_OTU_name)-1):
        #print i, idx_OTU_name[i]
        #print data[idx_OTU_name[i]]
        OTU_name = re.compile("(.*)(OTU_\d+)(;.*)").match(data[idx_OTU_name[i]]).group(2)
        data_annotator_dict[OTU_name]={}
        data_annotator_dict[OTU_name]["idx start"] = idx_OTU_name[i]
        data_annotator_dict[OTU_name]["idx end"] = idx_OTU_name[i+1]

    #print data_annotator_dict.keys()
    #sys.exit()

    #get required info for each otu on blast data subset
    for otu_n in data_annotator_dict.keys():
        #otu_n = 'OTU_359'

        data_subset = data[data_annotator_dict[otu_n]["idx start"]:data_annotator_dict[otu_n]["idx end"]]
        #print data_subset
        OTU_name = [line for line in data_subset if re.compile("(.*)(OTU_\d+)(;\d+)").match(line)][0]
        OTU_coverage = re.compile("(.*)(OTU_\d+;)(\d+)").match(OTU_name).group(3)
        data_annotator_dict[otu_n]["coverage"] = OTU_coverage
        #try:
        #    data_annotator_dict[otu_n]["filename"] = re.compile('(.*)(CCDB.*)').match(options.blastfile).group(2)
        #except:
        data_annotator_dict[otu_n]["filename"] = options.blastfile

        #print data_annotator_dict[otu_n]["filename"]
        #sys.exit()
        try:
            OTU_top_hit_title_line = [line for line in data_subset if re.compile(">[\s+]?(\w+)").match(line) and len(line) > 0][0]
        except StandardError,e:
            print e
            OTU_top_hit_title_line = "NoTopHit"
            keys = ["pid","sid","taxon", "score", "evalue", "reference sequence length", "number of gaps", "identity overlap reference sequence",
                    "identity percentage", "gap percentage","identity filter"]

            for k in keys:
                data_annotator_dict[otu_n][k] = "NoTopHit"
        #print len(OTU_top_hit_title_line), "'"+OTU_top_hit_title_line+"'"

        if OTU_top_hit_title_line != "NoTopHit":
            #print re.compile("(>\s+)(.*)(\|)(.+)(\|)(.+)").match(OTU_top_hit_title_line).group(2,4,6)
            #print OTU_top_hit_title_line.rstrip()
            n_vertical_bars = len(re.compile("\|").findall(OTU_top_hit_title_line.rstrip()))
            if n_vertical_bars == 2:
                OTU_processid, OTU_taxonomy, OTU_sampleid = re.compile("(>\s+|>)(.*)(\|)(.+)(\|)(.+)").match(OTU_top_hit_title_line).group(2, 4, 6)
                data_annotator_dict[otu_n]["pid"] = OTU_processid; data_annotator_dict[otu_n]["taxon"] = OTU_taxonomy; data_annotator_dict[otu_n]["sid"] = OTU_sampleid
            elif n_vertical_bars == 1:
                OTU_processid, OTU_taxonomy  = re.compile("(>\s+|>)(.*)(\|)(.+)").match(OTU_top_hit_title_line).group(2, 4)
                data_annotator_dict[otu_n]["pid"] = OTU_processid; data_annotator_dict[otu_n]["taxon"] = OTU_taxonomy; data_annotator_dict[otu_n]["sid"] = ""
            else:
                print OTU_top_hit_title_line
                sys.exit("ERROR: Make sure the title line contains at least 1 vertical '|' character to parse processID...")



            for i in range(0, len(data_subset)):
                if data_subset[i] == OTU_top_hit_title_line:
                    #  score and e-value
                    #print i
                    #print len(data_subset[i:len(data_subset)])

                    score_expect_line = [line for line in data_subset[i:len(data_subset)] if re.compile("(.+)(Score)").match(line)][0]
                    #print "Score Line" + score_expect_line, len(score_expect_line)
                    OTU_top_hit_score = re.compile("(.*Score.*\s+)(.*)(\s+bits)").match(score_expect_line).group(2)
                    OTU_top_hit_expect = re.compile("(.*Expect\s+=\s+)(.*)(\s*)").match(score_expect_line).group(2)
                    data_annotator_dict[otu_n]["score"] = OTU_top_hit_score; data_annotator_dict[otu_n]["evalue"] = OTU_top_hit_expect
                    score_ident_gaps_line = [line for line in data_subset[i:len(data_subset)] if re.compile("(.+)(Identities)").match(line)][0]

                    # identity
                    OTU_top_hit_identity_overlap2ref, OTU_top_hit_identity_refseqlen, OTU_top_hit_identity_percent = re.compile("(.*Identities\s+=\s+)(\d+)(\/)(\d+)(\s+\()(\d+\%)").match(score_ident_gaps_line).group(2,4,6)
                    data_annotator_dict[otu_n]["identity percentage"] = OTU_top_hit_identity_percent; data_annotator_dict[otu_n]["identity overlap reference sequence"] = OTU_top_hit_identity_overlap2ref
                    data_annotator_dict[otu_n]["reference sequence length"] = OTU_top_hit_identity_refseqlen
                    # gaps
                    OTU_top_hit_gaps, OTU_top_hit_gaps_percent = re.compile("(.*Gaps\s+=\s+)(\d+)(\/)(\d+)(\s+\()(\d+\%)").match(score_ident_gaps_line).group(2,6)
                    data_annotator_dict[otu_n]["number of gaps"] = OTU_top_hit_gaps; data_annotator_dict[otu_n]["gap percentage"] = OTU_top_hit_gaps_percent

                    OTU_top_hit_identity_float = float(re.compile('\d+').match(OTU_top_hit_identity_percent).group(0))
                    if OTU_top_hit_identity_float >= 99:
                        data_annotator_dict[otu_n]["identity filter"] = "species"
                    elif OTU_top_hit_identity_float >= 97 and OTU_top_hit_identity_float < 99:
                        data_annotator_dict[otu_n]["identity filter"] = "genus"
                    elif OTU_top_hit_identity_float >= 95 and OTU_top_hit_identity_float < 97:
                        data_annotator_dict[otu_n]["identity filter"] = "family"
                    elif OTU_top_hit_identity_float >= 90 and OTU_top_hit_identity_float < 95:
                        data_annotator_dict[otu_n]["identity filter"] = "order"
                    else:
                        data_annotator_dict[otu_n]["identity filter"] = "undetermined"

                    #print otu_n, score_expect_line, score_ident_gaps_line, OTU_top_hit_score,OTU_top_hit_expect

    for k in data_annotator_dict.keys():
        try:
            Final_Results_Dictionary[k] =  data_annotator_dict[k]
        except:
            pass

    #print data_annotator_dict["OTU_149"]
    #return (data_annotator_dict)

def WriteResultsOut(OTU_stats, outfile, meta_data):
    try:
        meta_columns_n = Counter([len(meta_data[k]) for k in meta_data.keys()]).most_common(1)[0][1]
    except:
        meta_columns_n=0
        pass

    num_lines = sum(1 for line in open(outfile))
    if num_lines == 0:
        fp = open(outfile, "w")
        header_line = "OTU\tCoverage(hits)\tTop_Hit_ProcessID\tTop_hit_SampleID\tTop_Hit_Identification\tScore(Bits)\t\
        E-value\tIdentity%\tOverlap\tRef_seq_length\tGap%\tGaps\tIdetity_filter"
        if meta_columns_n == 0:
            header_line = header_line +"\tFile_name\n"
        else:
            header_line = header_line +"\tBOLD_ProcessID\tBOLD_SampleID\t \
                    Identification\tPhylum\tClass\tOrder\tFamily\tGenus\tSpecies\tFile_name\n"
        fp.write(header_line)
        fp.close()


    fp = open(outfile, "a")
    for otu in OTU_stats.keys():
        meta_data_row = [""] * 10; idx_match=0
        if len(meta_data) > 0:
            #print OTU_stats[otu]["sid"]
            #print meta_data["SampleID"]
            try:
                #idx_match = [idx for idx in range(0, len(meta_data["SampleID"])) if meta_data["SampleID"][idx] == OTU_stats[otu]["sid"]][0] #match references based on sampleid
                idx_match = [idx for idx in range(0, len(meta_data["ProcessID"])) if meta_data["ProcessID"][idx] == OTU_stats[otu]["pid"]][0]  # match references based on processid
                meta_data_row=[ meta_data.get(key)[idx_match] for key in meta_data.keys()]
                while len(meta_data_row) < 10:
                    meta_data_row.append(" ")
                #print meta_data_row
                #print len(meta_data_row)
                #sys.exit()
            except:
                while len(meta_data_row) < 10:
                    meta_data_row.append("NoMatch")
                pass

        if OTU_stats[otu]["sid"] == "NoTopHit":
            meta_data_row =["NoTopHit"]*meta_columns_n
        #print OTU_stats[otu]
        fp.write("{!s}\t{!s}\t{!s}\t{!s}\
        \t{!s}\t{!s}\t{!s}\t{!s}\t{!s}\t{!s}\
        \t{!s}\t{!s}\t{!s}\t".format(otu, OTU_stats[otu]["coverage"],OTU_stats[otu]["pid"],\
                                        OTU_stats[otu]["sid"], OTU_stats[otu]["taxon"],\
                                        OTU_stats[otu]["score"], OTU_stats[otu]["evalue"], OTU_stats[otu]["identity percentage"],\
                                        OTU_stats[otu]["identity overlap reference sequence"], OTU_stats[otu]["reference sequence length"],\
                                        OTU_stats[otu]["gap percentage"], OTU_stats[otu]["number of gaps"], \
                                        OTU_stats[otu]["identity filter"]))

        for i in range(0, meta_columns_n):
            fp.write("{!s}\t".format(meta_data_row[i]))

        fp.write("{!s}\n".format(OTU_stats[otu]["filename"]))



def read_in_chunks(file_object, chunk_size=1024):
    """Lazy function (generator) to read a file piece by piece.
    Default chunk size: 1k."""
    while True:
        data = file_object.read(chunk_size)
        if not data:
            break
        yield data

def progress_bar(i, t, start_t, info_print_thresh):
    sys.stdout.write('  completed \r{:3.0f}%'.format(round(float(i + 1) / t * 100)))
    sys.stdout.flush()

    if i == info_print_thresh and info_print_thresh > 0:
        e_time = time.time() - start_t
        print " --> INFO: Estimated time to completion {:2.3f} hrs or {:2.3f}  min".format((float(e_time)/info_print_thresh)*float(t)/3600, (float(e_time)/info_print_thresh)*float(t)/60)

def read_tab_txt(f):
    with open(f, "rU") as f:
        reader = csv.reader(f, delimiter="\t");

        file_header = next(reader)
        #file_dictionary = OrderedDict()
        #OrderedDict([(i, i) for i in l])
        file_dictionary = OrderedDict([(col ,[]) for col in file_header])
        #file_dictionary = {col: [] for col in file_header}

        for row in reader:
            i=0
            for col in file_header:
                values = file_dictionary[col]
                values.append(row[i])
                file_dictionary[col]=values
                i+=1
    #print  file_dictionary["SampleID"]; print file_dictionary.keys(); sys.exit()
    return file_dictionary


def main():
    print "OTU Extractor From BLAST output v1.0\n"
    global options
    options = setupArguments()
    print options
    if options.resultfile == None:
        sys.exit("ERROR: define output file name (-o parameter)")

    meta_data_dict={}
    if options.metadatafile != None:
        print "metafile is defined! Time to read!"
        print options.metadatafile
        meta_data_dict = read_tab_txt(options.metadatafile)

    #print meta_data_dict
    fp = open(options.resultfile, "w"); fp.close()
    #open input file and convert all lines into list
    #print "Counting total number of lines in a file ..."
    #fp = open(options.blastfile, "r"); num_lines = sum(1 for line in fp); fp.close()  #num_lines = len(list(enumerate(fp)));


    #print num_lines; print range(0,num_lines, 10000)
    print "Extracting the OTU line file positions ..."
    OTULineNumbers=[]
    OTULineNumbersFilePositions = []
    #s_time  = time.time();
    #fp = open(options.blastfile, "rU")
    #for ln in range(0,num_lines):
    #for ln, line in enumerate(fp, 1):
        #line = linecache.getline(options.blastfile, ln)
    #    if re.compile("Query= OTU_.*").match(line):
    #            OTULineNumbers.append(ln)
    #    progress_bar(ln,num_lines,s_time, 1000000)

    #OTULineNumbers.append(num_lines)
    #print OTULineNumbers
    #fp = open(options.blastfile,"r")

    #i=0; nlines_cache = (num_lines/10)
    #while True:
    #
    #     next_n_lines = list(islice(fp, nlines_cache))
    #
    #     if not next_n_lines:
    #         break
    #     #print len(next_n_lines)
    #     for ln, line in enumerate(next_n_lines,1):
    #         ln = ln + i*nlines_cache
    #         if re.compile("Query= OTU_.*").match(line):
    #             OTULineNumbers.append(ln)
    #     print str(i)+"/"+str(int(num_lines/nlines_cache))
    #     i+=1




    ln = 1
    with open(options.blastfile, "rU") as fp:
        while True:
            fp_pos = fp.tell()
            line = fp.readline()
            if not line:
                OTULineNumbers.append(ln); OTULineNumbersFilePositions.append(fp_pos)
                break
            if re.compile("Query= OTU_.*").match(line):
                OTULineNumbersFilePositions.append(fp_pos)
                OTULineNumbers.append(ln)
            if ln % 1000000 == 0:
                print "Processed {:.1E} lines ...".format(Decimal(ln))
            ln += 1

    print "Found "+str(len(OTULineNumbersFilePositions))+" OTUs"

    fp.close()
    #print OTULineNumbers, len(OTULineNumbers)
    #print OTULineNumbersFilePositions, len(OTULineNumbersFilePositions)
    #fp.close()

    #sys.exit()
    #for num, line in enumerate(fp,1):
        #ln, line
        #bits = range(0, num_lines, 1000000)
        #bits.append(num_lines)
        #print bits[0]
    #    break

    #print len(OTULineNumbers)


    #with open(options.blastfile) as fp:
    #    raw_data = fp.readlines()
    s_time  = time.time(); raw_data = []
    print "Prasing BLAST output ...";
    #fp = open(options.blastfile, "r")
    #print len(OTULineNumbers)
    global Final_Results_Dictionary
    Final_Results_Dictionary = OrderedDict()
    for i in range(0, len(OTULineNumbers)):
        #print OTULineNumbers
        #print "Chunk "+str(i)+" out of "+str(len(OTULineNumbers)) + " ("+str(float(i)/float(len(OTULineNumbers))) +"%)"
        progress_bar(i,len(OTULineNumbers),s_time, int(len(OTULineNumbers)/10))
        if i+1 !=  len(OTULineNumbers):
            #print "started reading raw data"
            #for j in range(OTULineNumbers[i],OTULineNumbers[i+1], (OTULineNumbers[i+1] - OTULineNumbers[i])/3):
            #print range(OTULineNumbers[i],OTULineNumbers[i+1], (OTULineNumbers[i+1] - OTULineNumbers[i])/3)

            #raw_data = [linecache.getline(options.blastfile,ln) for ln in range(OTULineNumbers[i],OTULineNumbers[i+1])]
            #print "finished raw data reading"
            #raw_data = islice(fp, OTULineNumbers[i], OTULineNumbers[i+1]))
            #print len(raw_data)
            #sys.exit()
            #raw_data=[line.rstrip() for line in raw_data]
            #print "Number of lines: "+str(len(raw_data))
            #print raw_data[0]
            ParseTextInput(OTULineNumbers[i], OTULineNumbers[i+1], OTULineNumbersFilePositions[i], OTULineNumbersFilePositions[i+1])

    WriteResultsOut(Final_Results_Dictionary, options.resultfile, meta_data_dict)
    #fp.close()
    print "\nResults written to "+options.resultfile





if __name__ == "__main__":
    main()