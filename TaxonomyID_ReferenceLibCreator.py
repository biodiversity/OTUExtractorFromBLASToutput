from ete3 import NCBITaxa
import re, time
from subprocess import PIPE, Popen
from optparse import OptionParser

# https://github.com/zyxue/ncbitax2lin nice project with taxid to lineage conversion

def setupArguments():
    Parser = OptionParser()
    Parser.add_option('-i', action='store', dest='infile', help="input file to parse")
    Parser.add_option('-o', action='store', dest='outfile', help="path to tab-delimited results file")
    return Parser.parse_args()[0]

def getTaxid(url):
    try:
        http_response = Popen(["curl", "-s", url], stdout=PIPE).communicate()
        taxid = re.compile('(.*)(ORGANISM=)(\d+)').findall(http_response[0])[0][2]
    except:
        taxid = 0
    return taxid
if __name__ == "__main__":
    args = setupArguments()
    print args
    print args.infile
    fp_in = open(args.infile, "r")
    headers=["Input","Accession", "taxid","Lineage", "superkingdom", "kingdom" , "subkingdom", "phylum", "subphylum","class", "subclass",    "order", "suborder", "family", "genus", "species"]



    fp_out = open(args.outfile, "w")
    fp_out.write("\t".join(headers)+"\n"); fp_out.close()

    fp_out = open(args.outfile, "a")
    nlines = sum(1 for line in open(args.infile))
    for ln, line in enumerate(fp_in):
    #lines = [">gi|1051341410|ref|NR_138447.1|",">gi|1051341411|ref|NR_138448.1|"]
        print "{0:3.2f} % completed".format(float(ln)/float(nlines))

    #for line in lines:
        #line=">gi|1051341402|ref|NR_138441.1|"
        #print "Line #"+str(ln)
        tmp_results_dict = {h: "" for h in headers}
        #print line

        accession = re.compile('(.*\|){3,3}(.*)').match(line).group(1)
        accession = accession.rsplit("|")[0]
        tmp_results_dict["Accession"] = accession; tmp_results_dict["Input"] = line.rsplit()[0]

        url = "https://www.ncbi.nlm.nih.gov/nuccore/" + accession
        print url

        #print re.compile('(.*)(ORGANISM=)(\d+)').findall(http_response[0])
        #sys.exit()
        taxid = 0

        for i in range(0,10):
            taxid = getTaxid(url)
            if taxid >0:
                break
            time.sleep(10)



        tmp_results_dict["taxid"] = taxid
        #taxid = re.compile('(.*)(ORGANISM=)(\d+)').findall(http_response[0])[1][2]

        #print "TaxonomyID: " + str(taxid)
        taxon_dict={}

        try:
            if taxid > 0:
                taxon_dict = NCBITaxa().get_rank(NCBITaxa().get_taxid_translator(NCBITaxa().get_lineage(taxid)).keys())
                taxon_dict = {v: k for k, v in taxon_dict.iteritems()}  # invert dictionary
                taxon_dict = {k: NCBITaxa().get_taxid_translator([v]).values()[0] for k, v in taxon_dict.iteritems()}
                #print taxon_dict
                lineage_full = [NCBITaxa().get_taxid_translator([i]).values()[0] for i in NCBITaxa().get_lineage(taxid)][2:]
                #print lineage_full
                tmp_results_dict["Lineage"] = ";".join(lineage_full)
                for k in taxon_dict.keys():
                    tmp_results_dict[k] = taxon_dict[k]

        except StandardError,e:
            print e


        try:
            for k in headers:
                try:
                    #print tmp_results_dict[k]
                    fp_out.write(tmp_results_dict[k]+"\t")
                except:
                    pass
            fp_out.write("\n")
        except:
            pass

    fp_out.close()


    #>gi|1041581905|ref|NR_137453.1"

