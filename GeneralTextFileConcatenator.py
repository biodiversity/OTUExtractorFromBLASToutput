__author__ = 'kbessonov'
__maintainer__ = "Kirill Bessonov"
__email__ = "kbessonov@gmail.com"
__copyright__ = "UoG"

import sys,glob, os, linecache
from optparse import OptionParser
from collections import Counter

def setupArguments():

    Parser = OptionParser()
    Parser.add_option('-d', action='store', dest='dir', help="Absolute path to directory")
    Parser.add_option('-e', action='store', dest='extension', help="file extension or last letters (improves filtering)")
    Parser.add_option('-o', action='store', dest='file_out', help="Output concatinated file name")


    return Parser.parse_args()[0]


if __name__ == "__main__":
    print "Welcome to header-based concatinator"
    args = setupArguments()
    #dirfiles = [f for f in os.listdir(args.dir)]
    dirfiles = glob.glob(args.dir+"/*"+args.extension)
    print dirfiles

    print str(len(dirfiles))+" files in input dir: "+str(", ".join(dirfiles)+"\n")

    headers=[]
    for f_in in dirfiles:
        print f_in
        try:
            open(f_in, "r")
        except IOError:
            sys.exit("ERROR: Could not open \""+str(f_in)+"\" file. Check input path (parameter -d)")
        h=linecache.getline(f_in, 1)
        headers.append(h)
    #print headers
    #print linecache.getline("./RESULTS/CCDBION219_NGS1_Diatoms_2016-04-05-CYA359F-781R-deg_IonXpress_004.fastq.fasta_BLAST.txt.out", 1)

    common_header = Counter([h for h in headers]).most_common(1)[0][0].rstrip()
    print common_header
    #if len(common_header) < 5:
    #    sys.exit("ERROR: Files do not contain a COMMON header on line 1! Or input directory does not end with \"/\" symbol")
    #print Counter([h for h in headers]).most_common(1)

    fp_out = open(args.file_out,"w"); fp_out.write(common_header+"\tfile_name\n"); fp_out.close()

    fp_out = open(args.file_out, "a")
    for f_in in dirfiles:
        header = linecache.getline(args.dir+f_in, 1)

        n_lines = sum(1 for line in open(f_in))
        print "Concatenating \""+f_in+ "\" file to \""+str(fp_out.name)+"\""
        [fp_out.write(linecache.getline(f_in,ln).rstrip()+"\t"+f_in+"\n") for ln in range(2, n_lines)]

    print "\nConcatenated "+str(len(dirfiles))+" "+args.extension+" files found at \""+str(args.file_out)+"\""
    fp_out.close()