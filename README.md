## OTU BLAST Output parser and annotatator
Parses the output of the BLAST program and tries to summarize meta information on each OTU. 
The summary includes information on the number of sequences representing a given OTU (i.e. coverage), 
bits score, %identity, %gaps and other metrics of the top hit

### Install
Download and install python 2.7 (if required). Launch the script such as below

```
 python ${root_dir}/OTUBlastParser.py -i  input_blast_results.txt -r  BOLD-taxonomy-2017-06-13.txt -o results.txt
```
### Usage
The program requires just 2 input argmuments: path to the input and output files. The optional meta data file
can be supplied to add meta taxonomic information from BOLD. 

**Note:** the optional meta file should contain the header with the `SampleID` column. 
Names of other columns are not relevant

```
Usage: OTUBlastParser.py [options]

Options:
  -h, --help       show this help message and exit
  -i BLASTFILE     BLAST input file to parse
  -o RESULTFILE    Tab-delimited results file
  -r METADATAFILE  Meta data file with taxonomy info per sample ID (optional)
```

### PROBLEM
1. Each raw BLAST output file contains a long list of results (hits) for each OTU
2. Need to parse unique OTU names and gather statistics
3. For each OTU map number of hits and select the "top" hit based on score and %identity
4. Extract OTU info from calculations for each input blast result file
4. Output in a format of a tab-delimited table

### TODO:
- [ ] min blast score threshold
- [x] add 'identity filer' column
    - \>=99\% identitty  = species
    - \>=97\% \< 99 \% indentity = genus
    - \>=95\% \< 97 \% indentity = family
    - \>=90\% \<95\% identity = order
    - \<90\% identity = not known
- [x] add coverage info of the OTU even if there is no blast hit
- [x] add filename column with edited filname that starts with CCDB....txt
- [x] concatinate parital results files into a single file (added as a separate script 'ConcatenatorResults.py')


### History
#### June 7, 2017
Create tab-delimited file with the following columns
OTU name / Coverage (number of hits) / Top hit Family or Genus and Species / ProcessID / SampleID / Score (bits) / Identity / Gaps

#### March 22, 2017
Min score filter option (300)
Remove duplicates.

Example of the tab-delimited summmary ouput

```
OTU_1000;100  Identification Score Expect Identities Gaps File_name

```
